﻿\section{Contexte}
\subsection{L'institut des sciences cognitives}
\paragraph{} L'\gls{ISC} est un institut dépendant du \gls{CNRS}. L'\gls{ISC} est composé de deux départements: le département de <<\emph{Neurosciences Cognitives (UMR 5229)>>} 
et le département de <<\emph{Langage Cerveau et Cognition (UMR5304)>>} \cite{ISC00}.
\subsection{L'équipe}
\paragraph{}L'équipe sur le projet de recherche de l'entraînement de l'attention est une équipe dirigée par \bsc{Suliann Ben Hamed}. Elle donne la 
direction du projet et les pistes de recherches. Nous avons également \bsc{Simon Clavagnier}, post doctorant, qui est responsable du développement des outils et protocoles de l'expérience. Il
est assisté par \bsc{Camilla Ziane} qui fait sa thèse sur l'entraînement de l'attention. Elle développe le projet et analyse ses résultats. Enfin, \bsc{Gwendoline Gomez} et
moi même sommes ici pour adapter l'expérience en jeu sérieux.
\subsection{Projet en cours et définitions}
\label{sssec:def}
\paragraph{} Le projet sur lequel travaille l'équipe porte sur l'attention. Avant d'expliquer en quoi consiste le projet, il convient de définir l'attention.\\
Selon \bsc{Larousse}\footnote{\url{https://www.larousse.fr/dictionnaires/francais/attention/6247?q=attention\#6230}}, 
L'\emph{attention} est définie dans le langage courant comme << la capacité de concentrer volontairement son esprit sur un objet déterminé>>.
Toutefois, la définition rapportée dans le mémoire de \bsc{Emma COUSINIER}, \bsc{Amale ZEMMAHI} et \bsc{Quentin ADRIAN,}\cite[p.~5-9]{MEM00} nous semble plus utile dans notre contexte.\\
Selon eux, l'\emph{attention} est la capacité de <<prioriser>> le traitement des informations perçus par nos sens afin de se focaliser sur les plus importantes pour notre objectif.
Nous appellerons ces informations \emph{cible} en opposition aux autres informations non utiles que nous appellerons \emph{distracteurs}.
\paragraph{} Au vu de cette première définition, nous pouvons préciser l'\emph{attention} en <<sous-groupes>>:
\begin{description}
    \item[Attention sélective] Il s'agit de notre capacité, comme dit plus haut, de se concentrer sur des informations pertinentes vis-à-vis de notre \emph{cible}.
    Cette forme d'attention est la base des autres. Elle a pour fonction d'amplifier de façon sélective la masse d'information qui provient de nos sens 
    (vision, ouïe, odorat, touché...) de façon à privilégier les informations nécessaires à nos objectifs, comme montrés dans la figure~\ref{fig:selective_attention}.
    \begin{figure}[H]
        \includegraphics[width=\textwidth]{selective_attention.png}
        \caption{Attention sélective}
        \label{fig:selective_attention}
    \end{figure}
    Dans la figure~\ref{fig:selective_attention}, l'image de droite nous donne une représentation de l'attention sélective: seuls quelques bâtiments, nos \emph{cibles}, 
    sont clairs et nets. Le reste nous apparaît flouté, il agit des \emph{distracteurs}. À noter que nos \emph{cibles} sont déterminées en fonction de processus intentionnels
    (<<trouvez Charlie>>), comme non intentionnels (pour reprendre les mots de \bsc{Simon Clavagnier}, <<on remarque de suite le tigre affamé qui nous fonce dessus 
    dans la forêt>>).
    
    \item[Attention continue] L'attention continue est le nom donné au fait de maintenir son attention focalisée sur la même cible dans le temps.
    Ci-dessous, la figure\ref{fig:sustained_attention} donne une représentation de l'attention continue. On peut clairement voir que c'est un exercice <<au cours du temps>>:
    il y a un <<engagement>> de l'attention qui sera maintenue sur une durée avant de se <<désengager>>.
    \begin{figure}[H]
        \includegraphics[width=\textwidth]{sustained_attention.png}
        \caption{Attention continue}
        \label{fig:sustained_attention}
    \end{figure}

    \item[Attention divisée] Cette forme consiste à placer notre attention sur plusieurs \emph{cibles} simultanément (traitement en parallèle de plusieurs types d'informations).
    L'exemple type étant la "cocktail partie" où on est capable (mais on peut en discuter....)  de suivre plusieurs conversation en même temps.

    \item[Attention executive] Il s'agit de lier une \emph{cible} à une réponse. C'est à dire de porter son attention sur la prise de décision pour s'adapter au contexte.
    Par exemple, se retenir de signaler les \emph{cibles} 
    tout en signalant tous les \emph{distracteurs}.
\end{description}

\paragraph{}Nous pensons qu'il existe une relation entre la durée et la difficulté de la réalisation d'un objectif (que nous appellerons \emph{tâche}) et l'attention
que le sujet exerce sur celle-ci. Le schéma de la figure~\ref{fig:difficulty_attention} tente d'expliquer comment nous pouvons augmenter cette durée. 

Selon la littérature existante\cite{BOOSMA1998}, il s'agit du plus dure à entraîner car on aurait tous une limite biologique. Nous pensons toutefois que nous pouvons 
influer sur la durée (axe des ordonnées du graphique) en jouant sur la difficulté de la \emph{tâche} (axe des abscisses). Si la \emph{tâche} est trop dur (overload), 
le sujet est plus susceptible de l'abandonner rapidement alors qu'inversement, si elle est trop facile (underload), il va rapidement s'en désintéresser. Pour maximiser 
le temps passé sur la \emph{tâche}, nous cherchons donc à la rendre suffisamment stimulante mais pas trop non plus. Cela dépends de saillance de la \emph{tâche} et de 
la motivation du sujet.

Ainsi, pour un entraînement efficace nous devons avoir une \emph{tâche} ni trop facile, ni trop difficile, de façon à avoir une marge d'apprentissage et d'amélioration.
Nous possédons deux levier différents que nous devons utiliser: la saillance et la motivation.

\begin{figure}[H]
    \includegraphics[width=\textwidth]{difficulty_attention.png}
    \caption{Relation entre la difficulté et l'attention}
    \label{fig:difficulty_attention}
\end{figure}



\subsection{Projet}
Notre projet consiste à créer un entraînement de l'attention sous la forme d'un jeu sérieux. Ce format nous permet de facilement interagir avec le graphique présenté dans
la figure~\ref{fig:difficulty_attention}. D'abord, le jeu est par essence motivant pour les gens et en particulier les jeunes. Cette motivation peut être affecté au travers
d'un scénario, d'un challenge, de retours et récompenses et les jeux nous fournissent ces mécaniques. Ensuite, la difficulté du jeu peut s'adapter en fonction des performances
du joueur. Nous pouvons ainsi nous placer dans un rang de difficulté acceptable mais non fixe (entre <<underload>> et <<overload>> sur le graphique).

\newpage