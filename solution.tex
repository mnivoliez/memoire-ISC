﻿\section{Solution}

Nous retranscrirons ici le processus par lequel nous avons abouti à la solution actuelle. Cette partie montrera comment nous avons évalué le besoin, 
quels outils nous avons mis en place pour planifier, supporter, monitorer et revenir sur ce que nous avons développé pour y répondre.

\subsection{Etude préliminaire du besoin}

Avant de commencer le développement du jeu, il convenait de définir en premier lieu les objectifs et contraintes du projet. Bien sûr, nous avions déjà quelques idées liées à la 
description de base du projet. Nous avons alors pratiqué l'expérience (voir section~\ref{sec:existing}) ce qui nous a permis d'affiner ou d'invalider nos premières idées.
Idées que nous pu confronter à celles de \bsc{Suliann Ben Hamed}, \bsc{Simon Clavagnier} et \bsc{Camilla Ziane}. De cette confrontation sont ressortis différents éléments.
\begin{enumerate}
    \item Il faut que le jeu soit attractif afin de générer chez le joueur la volonté de revenir. C'est essentiel dans l'objectif d'entraînement.
    \item Il faudrait aussi diviser le jeu en différents <<mondes>> ciblant chacun un type d'\emph{attention} (voir section~\ref{sssec:def}).
    \item Il faut que le jeu ait une difficulté adaptative afin de pouvoir rester entre l'<<underload>> et l'<<overload>> 
    (voir figure~\ref{fig:difficulty_attention}) en fonction de la performance du joueur.
    \item Comme nous visons le milieu scolaire, le jeu doit permettre une interaction avec les professeurs et une mise en accord avec les sujets de cours.
    \item De même, il est pertinent de permettre aux joueurs de jouer en équipe pour des raisons d'émulsion et d'attractivité (levier de motivation, voir 
    section~\ref{sssec:def}).
    \item Enfin, les données relatives aux joueurs et à leurs parties doivent être récupérées et être exploitables par l'équipe de recherche.
\end{enumerate}

\subsection{Premières propositions}
Après cette étude, nous avons formulé différentes propositions aux chercheurs. Nous avons finalement convenu que la meilleure option serait de réaliser
un jeu composé d'un ensemble de mini jeux afin de pouvoir orienter chaque mini plus vers une forme d'attention (voir section~\ref{sssec:def}).
Ainsi, nous avons proposé un mini jeu, ou <<monde>>, par type d'attention. Chacun de ces jeux sera ensuite découpé en \emph{niveau} de difficulté
(voir figure~\ref{fig:game_level_order}). Le passage d'une difficulté à une autre et d'un monde à l'autre étant une question en suspend, nous proposons
de le rendre paramétrable afin de pouvoir essayer l'ensemble des possibilités et déterminer la meilleure stratégie avec les résultats.
\begin{figure}[H]
    \includegraphics[width=\textwidth]{game_level_order_v2.png}
    \caption{Ordre des niveaux}
    \label{fig:game_level_order}
\end{figure}
Ce découpage de la difficulté nous permettra ainsi de mettre en place des \emph{boucles de Gameplay}, générant l'attractivité du jeu auprès du joueur.
Nous appelons \emph{boucle de Gameplay} une suite logique au sein d'un jeu respectant la suivante:
\begin{enumerate}
    \item Un élément déclenche la boucle (un dialogue, un évènement, autre).
    \item Le joueur doit effectuer une suite d'action (péripéties) pour compléter la boucle.
    \item À la fin de la boucle, le joueur reçoit une récompense, quelle qu'en soit la forme.
\end{enumerate}
Dans notre cas, le déclencheur est le début du mini jeu, le joueur ensuite doit effectuer la tâche, <<son entraînement>>, puis nous lui donnons un retour (audio, score, autre)
afin de le récompenser.
En prenant en compte ces points et le fait qu'il était important pour nous que les mini jeux reprennent les tâches existantes (voir section~\ref{sec:existing}), nous avons
fait les propositions suivantes:

\begin{wrapfigure}{r}{0.5\textwidth}
    \centering
    \includegraphics[width=0.48\textwidth]{miniPipeConcept.jpg}
    \caption{Concept du Mini Pipe}
    \label{fig:mini_pipe_concept}
\end{wrapfigure}
\paragraph{Mini pipe} Dans ce jeu, des objets tombent d'un tube à un autre. Le joueur doit enlever les <<mauvais>> objets (\emph{cibles}). Ce jeu reprend la tache du 
\emph{CPT} et convient parfaitement comme premier candidat. Il nous permet de travailler sur l'attention continue (voir section~\ref{sssec:def}).
Dans ce jeu, nous avons plusieurs leviers pour moduler la difficulté: \\
\begin{itemize}
    \item La distance entre les tuyaux permet de jouer sur le temps de visibilité de l'objet qui tombe.
    \item Le <<contraste>> entre les \emph{cibles} et les \emph{distracteurs} demandant ainsi une plus grande attention afin de les différencier.
    \item Le mouvement du ou des tuyaux.
\end{itemize}

\begin{wrapfigure}{r}{0.5\textwidth}
    \centering
    \includegraphics[width=0.48\textwidth]{drones.jpg}
    \caption{Concept du jeu Drones}
    \label{fig:drones_concept}
\end{wrapfigure}
\paragraph{Drone} Dans ce jeu, le joueur est un architecte devant construire des bâtiments. Afin de les construire, des drones lui apportent des matériaux.
Le joueur doit alors sélectionner le bon drone. De plus, certains bâtiments requièrent la présence d'autre bâtiment pour exister. Ici aussi, nous possédons plusieurs leviers de
difficultés.
\begin{itemize}
    \item Le mouvement des drones.
    \item Le contraste des drones.
    \item Le nombre de drones affiché à l'écran (foule de drones).
    \item La complexité des éléments à construire.
\end{itemize}

\begin{wrapfigure}[17]{r}{0.5\textwidth}
    \centering
    \includegraphics[width=0.48\textwidth]{spaceship.jpg}
    \caption{Concept du jeu Spaceship}
    \label{fig:spaceship_concept}
\end{wrapfigure}
\paragraph{Spaceship} Ce jeu met en scène un vaisseau dans un <<champ>> d'astéroïdes, à la recherche de survivant d'un autre vaisseau. Notre vaisseau est équipé de boucliers
permettant de le protéger des divers objets spatiaux qui sont susceptibles de l'endommager. Ces boucliers, au nombre de trois, devront être activés par le joueur au moment 
opportun. Leur activation est limitée dans le temps et ils ont un <<temps de rechargement>> entre deux activations. De plus, s'ils sont activés, ils empêcheront la récupération
des potentiels survivants. \\

\begin{wrapfigure}[11]{r}{0.5\textwidth}
    \centering
    \includegraphics[width=0.48\textwidth]{hackerConcept.jpg}
    \caption{Concept du jeu du Hacker}
    \label{fig:hacker}
\end{wrapfigure}
\paragraph{Hacker} Ici, le joueur est un \emph{Hacker} devant décrypter un code. Il devra trouver des motifs particuliers dans une grille avant la fin du temps imparti.
Nous gérons ici la difficulté au travers du nombre de motifs dans la grille et leur durée d'apparition.
\paragraph{Adaptation au milieu scolaire} Les mini jeux devront ensuite être adapté au milieu scolaire et permettre une personnalisation liée à la matière. Pour se faire, nous avons imaginé plusieurs 
<<scénarios>>.
Ces scénarios sont représentés dans la figure~\ref{fig:scenarios}.
\begin{figure}[H]
    \includegraphics[width=\textwidth]{scenarios.png}
    \caption{Scénarios}
    \label{fig:scenarios}
\end{figure}

Chacun de ses scénarios implémentera et personnalisera les mini-jeux décrits précédemment. Par exemple, le mini jeu du \emph{hacker} pourra être adapté pour des cours de
langues.

\paragraph{Intégration du professeur} En parallèle de cela, le professeur devra aussi être inclus dans l'évolution du jeu. Nous avons donc envisager la création d'une interface permettant d'inférer
dans le graphe d'évolution d'un joueur (voir figure~\ref{fig:game_level_order} et figure~\ref{fig:scenarios}) afin adapter l'entrainement attentionnel de l'élève au besoin 
de celui-ci.

\paragraph{Multijoueur}Enfin, une dimension multijoueurs fut évoquée par l'équipe scientifique. Nous avons alors imaginé un mode de jeu en équipe où chaque joueur 
se verra proposer un mini jeu dont dépendra la progression de l'équipe.
Par exemple, chaque joueur peut incarner un rôle différent (médecin, hacker, architecte, etc.) et devra résoudre un mini jeu relatif à son scénario dans un contexte d'équipe.
Le hacker pourrait avoir à décrypter un code pour ouvrir une porte que l'architecte répare grâce à des drones pendant que le médecin soigne tout ce beau monde.
Si tous les joueurs parviennent à obtenir un suffisamment bon score, le niveau sera fini et le scénario multijoueurs continuera.
Ceci étant dit, un point doit être abordé ici. Les joueurs au sein d'une même équipe n'auront pas tous le même niveau. Un équilibrage doit ainsi être réalisé.
Pour se faire, si un joueur <<A>> est plus performant que le joueur <<B>>, nous allons faire en sorte que <<A>> ai des sorte de <<quêtes secondaires>> qui viendrons
aider <<B>>. Ces quêtes peuvent se traduire par des \emph{cibles} spécifique dans le jeu de <<A>> et leur impact sera visible dans celui de <<B>>, par exemple au travers
d'animation ou de bonus accordés au joueur.

\subsection{Mise en place de l'environnement de travail}

Une fois le périmètre du projet établi, nous avons dû mettre en place un environnement permettant de répondre aux besoins suivants:
\begin{description}
    \item[Contenir le code] Le code du jeu se doit de résider à un emplacement accessible à tout membre du projet.
    \item[Contenir de la documentation] Des ressources utiles à la création du jeu (papiers de recherches, analyses, définitions, autres) ou à son utilisation 
    (manuel, wiki, autre), en passant par son évolution, doivent être accessibles et modifiables facilement pour tout membre du projet.
    \item[Gestion du projet] Nous devons avoir un outil permettant de planifier les étapes de développement, les préciser si besoin, et gérer le planning qui en découle.
    \item[Retour sur le jeu] Nous devons permettre des retours rapides sur le jeu.
\end{description}

Afin de répondre à ces besoins, nous avons mis en place l'outil \emph{Gitlab}\footnote{\url{https://about.gitlab.com/}}.
Cet outil nous propose de stocker le code du jeu au travers du \gls{vcs} \emph{Git}\footnote{\url{https://git-scm.com/}}. Il met également à notre disposition un wiki permettant
d'archiver la documentation autour du projet, ainsi qu'une interface de gestion de projet par ticket nous permettant de fixer des objectifs et de les suivre durant leur 
réalisation. Ci-dessous un extrait de l'interface de \emph{Gitlab}.
\begin{figure}[H]
    \centering
    \includegraphics[height=15cm]{gitlab_serious_ecas.png}
    \caption{Page Gitlab du projet}
    \label{fig:gitlab_serious_ecas}
\end{figure}

Enfin, au travers de son extension \emph{Gitlab Runner}, nous pouvons ordonner les tests du code, la construction automatique de l'application (intégration continue)
et même son déploiement automatique (déploiement continu).
C'est grâce à ses outils que nous pouvons mettre en place des cycles de développement courts et ainsi instaurer une discussion permanente avec les chercheurs autour du jeu et 
de son avancement.
Afin de mettre en place cet environnement, nous avons eu le concours de l'administrateur réseau \bsc{Sylvain Maurin} qui nous a mis à disposition trois serveurs:
\begin{itemize}
    \item Un serveur \emph{maître} sur lequel se trouve \emph{Gitlab} et le déploiement actuel du jeu.
    \item Deux serveurs \emph{esclaves} qui testent et construisent le jeu dès que le code ou les \glspl{asset} du jeu sont modifiés.
\end{itemize}

Vous trouverez dans la figure~\ref{fig:network_schema} un schéma réseau simplifié de notre architecture.
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{network_schema.png}
    \caption{Schéma réseau simplifié}
    \label{fig:network_schema}
\end{figure}

À ce jour, le jeu est construit, ou compilé, pour les plateformes Linux, Windows et WebGL selon le schéma suivant:
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{IC-DC.png}
    \caption{Intégration et déploiement continu}
    \label{fig:icdc}
\end{figure}

\subsection{Organisation du travail}

L'environnement de travail décrit dans la section précédente fait partie d'un ensemble appelé \emph{DevOps}. Il s'agit d'un ensemble d'outils, de pratiques et de personnes
qui ont pour but de réduire la distance entre la partie développement et la partie opérationnelle sur un projet. De manière plus générale, il faut voir cela comme des pratiques
et outils permettant à des personnes de mettre en place des boucles de développement plus ou moins courte et qui intègre les retours utilisateurs et clients de 
manière constante et efficace (voir figure~\ref{fig:devops}).
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{devops.png}
    \caption{Schéma explicatif du DevOps}
    \label{fig:devops}
\end{figure}

Dans cette section, nous allons parler des méthodes s'appuyant sur l'outillage mis en place à la section précédente.

\paragraph{Ticket}Tout tournes autour des tickets. Un ticket est un outil permettant de décrire une tâche à réaliser, un incident, un objectif ou tout ce qui doit être 
traité par l'équipe projet. Le ticket possède un titre, une description et éventuellement, une personne assignée et durée de réalisation du ticket estimé. Dans notre cas, il 
s'agit également d'un point de discussion entre développeurs et chercheurs. Dans le cas de \emph{Gitlab}, les tickets seront aussi liés au code, permettant ainsi de faire le 
lien entre la tâche conceptuelle et sa réalisation concrète. La description, et les commentaires, du ticket permettent de définir l'objectif à atteindre, de planifier comment 
le réaliser et également d'en définir les limites et contraintes. De fait, c'est un outil de planification du travail essentiel à l'équipe.

\paragraph{Réalisation}Une fois le ticket créé, la réalisation de celui-ci peut commencer. La première étape est de créer une <<branche>> dans le \gls{vcs} afin de
\emph{compartimenter} les modifications et ainsi ne pas impacter la réalisation d'un autre ticket. Cela permet, entre autres, de  facilement visualiser les changements
effectués sur le projet et éventuellement revenir dessus, voir les annuler. À chaque fois que l'avancer du développeur sera sauvegarder sur le serveur \emph{Gitlab}, 
les \emph{runners} se chargeront de tester le projet et de s'assurer de sa robustesse.

\paragraph{Revue}Lorsque la réalisation du ticket est jugée suffisante et satisfaisante par la personne assignée à celle-ci, une revue du travail accompli est mise en place 
afin de valider les changements.

\paragraph{Construction et déploiement} Une fois la revue validée, le projet est construit par les \emph{runners}, puis la version \emph{WebGL} sera déployé sur 
le serveur \emph{maître}. Les chercheurs pourront alors tester le jeu et nous faire des retours sur celui-ci au travers des tickets ou de manière orale.
Ces retours sont un moteur important dans le développement du jeu.

\paragraph{Documentation}Dans le même temps que les éléments présentés précédemment, la documentation est essentielle. Pour pouvoir expliquer en quoi, il faut d'abord 
identifier les différents types de documentation. 
\begin{description}
    \item[Scientifique] Il s'agit de la documentation sur laquelle repose notre travail. Elle explicite les principes conceptuels sous-jacents au jeu et nous indique les
    limites et contraintes du jeu. Également, cette documentation nous permet de déterminer la marche à suivre lors de la réalisation du jeu. Cette documentation est
    majoritairement fournie par les scientifiques.
    \item[Code] Il s'agit de documenter le code produit afin de permettre aux autres développeurs de comprendre celui-ci plus facilement. Usuellement, cette documentation
    est sous la forme de commentaire dans le code.
    \item[Utilisation] Cette documentation s'adresse aux utilisateurs du jeu et aux exploitants des données de celui-ci. Elle explicite comment l'utilisateur interagit avec
    le jeu.
\end{description} 

\subsection{Développement du jeu}
\label{ssec:dev_game}

La première étape vers un jeu fonctionnel était de proposer au chercheurs un prototype. Nous sommes donc partis sur le mini jeu du \emph{Mini pipe} 
(voir figure~\ref{fig:mini_pipe_concept}) dans la mesure où il semblait être le plus simple et rapide à réaliser, et qu'il permettait de tester presque 
toutes les formes d'attentions (voir section~\ref{sssec:def}).
\begin{wrapfigure}[20]{l}{0.34\textwidth}
    \centering
    \includegraphics[width=0.32\textwidth]{folder_tree.png}
    \caption{Organisation des fichiers du projet}
    \label{fig:folder_tree}
\end{wrapfigure}
Nous avons commencé par établir un catalogue de données à partir du \emph{CPT} existant (voir section~\ref{sssec:cpt}). Ces données sont importantes, car elles définissent
une orientation dans la manière de développer le jeu.
Nous avons ensuite défini la structure de projet (voir figure~\ref{fig:folder_tree}). Les prérequis pour cette structure étaient d'être relativement simple pour permettre un 
développement rapide et être facilement compréhensible pour d'autres potentiels développeurs.

\paragraph{}Dans la figure~\ref{fig:folder_tree}, vous pouvez observer un dossier \emph{Editor}. Ce dossier permet de décrire des ce que l'éditeur \emph{Unity} doit faire. Nous avons 
d'abord un \emph{BuildCommand} indiquant à \emph{Unity} comment il doit construire le jeu et comment il doit exécuter les tests unitaires. Ces tests se trouvent dans le 
dossier \emph{EditModeTests}. Ces tests seront passés par l'éditeur soit travers de sont interface de test (voir figure~\ref{fig:unity_test_runner}), soit lors de 
l'\emph{intégration continue} (voir figure~\ref{fig:icdc}).
\newpage

\begin{wrapfigure}{R}{0.52\textwidth}
    \centering
    \includegraphics[width=0.5\textwidth]{unity_test_runner.png}
    \caption{Interface de test d'Unity}
    \label{fig:unity_test_runner}
    \vspace{-30pt}
\end{wrapfigure}
\paragraph{}Par souci de rapidité et de simplicité, nous avons limité les tests aux parties du code <<critiques>> tel que le calcul du \emph{dprime} 
(voir section~\ref{sssec:exist_data_process}) ou encore l'envoi des données.

\paragraph{}Une fois cette structure posée, nous avons mis en place un fichier \emph{gitlab-ci.yml} permettant d'indiquer à \emph{Gitlab} comment tester et construire le jeu.

\begin{wrapfigure}[21]{L}{0.46\textwidth}
    \centering
    \includegraphics[width=0.44\textwidth]{proto_pipe_concept_detail.jpg}
    \caption{Détail du concept du prototype}
    \label{fig:proto_pipe_concept_detail}
\end{wrapfigure}
\paragraph{}Nous avons alors commencé le code. Dans cette version, le joueur commence la partie en appuyant sur un bouton <<start>>. Des boules commencent alors à tomber. Les boules bleu foncé sont des \emph{distracteurs} et
les boules bleu clair sont des \emph{cibles}. Le joueur appuie sur la barre <<espace>> quand il voit une \emph{cible}. S'il y en avait effectivement une, elle viendra grossir
son score. Sinon, le score du joueur diminuera. En parallèle, nous comptabilisons les \emph{Hit} et \emph{Miss} du joueur et nous calculons son \emph{dprime}. En fonction de 
l'évolution de ce dernier, nous adaptons la difficulté du jeu. Pour gérer cette difficulté, nous avons mis en place 3 mécaniques:
\begin{itemize}
    \item Le tuyau du bas remonte afin de diminuer le temps de visibilité des boules.
    \item Les tubes tournent perturbant ainsi la vision du joueur.
    \item Les tubes <<vibrent>> afin de toujours troubler la vision du joueur.
\end{itemize}
\newpage

Nous sommes partis sur un visuel simple et clair (fortement inspiré de Mario) dont un exemple est visible dans la 
figure~\ref{fig:proto-pipe}.
\begin{figure}[H]
    \centering
    \includegraphics[height=7cm]{proto-pipe.png}
    \caption{Visuel du prototype}
    \label{fig:proto-pipe}
\end{figure}

\paragraph{Premiers retours}Après avoir montré cette version aux chercheurs, il nous a été demandé de rajouter plusieurs tuyaux afin de demander au joueur de travailler
son attention sur plusieurs points spatiaux. Cela nous a demandé de revoir notre système de détection des boules afin de le rendre facilement réutilisable et 
surtout dissociable. Nous avons donc abouti à une nouvelle version visible figure~\ref{fig:proto-pipe-multi}.

\begin{figure}[H]
    \centering
    \includegraphics[height=7cm]{proto-pipe-multi.png}
    \caption{Visuel du prototype avec plusieurs tuyaux}
    \label{fig:proto-pipe-multi}
\end{figure}

Par contrainte de temps, nous avons également décliner le ce premier mini jeux afin de pouvoir représenter chaque \emph{mondes} (voir figure~\ref{fig:scenarios}).
Ces déclinaisons sont visible dans la figure~\ref{fig:proto-pipe-difficulty-evolv}. Nous pouvons y l'entraînement à l'attention continue (gauche), sa modulation via 
l'orientation du tube (milieu), et l'entraînement l'attention divisive (droite).

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{proto-pipe-difficulty-evolv.png}
    \caption{Visuel du prototype, évolution de la difficulté}
    \label{fig:proto-pipe-difficulty-evolv}
\end{figure}

\paragraph{Vers une version plus finalisée}Bien que le jeu permette l'entraînement de l'attention en l'état, il ne représente pas suffisamment d'attrait pour le joueur pour
planifier une récurrence suffisante. Nous avons donc décidé de travailler sur apparence du jeu afin de générer l'envie du joueur envers celui-ci.
Nous avons choisi, au vu des tendances cinématographiques et du caractère adaptatif de notre jeu, de nous diriger vers un thème futuriste.

\begin{figure}[H]
    \centering
    \includegraphics[height=7cm]{minipipe-futurist.jpg}
    \caption{Schéma de la version 2 du mini pipe}
    \label{fig:minipipe-futurist}
\end{figure}

Pour réaliser cette mise à jour vers un style futuriste, nous nous sommes inspirés de grand nom de la science-fiction dans le jeu vidéo comme \emph{Half Life 2}, duquel nous
avons pris les générateurs d'énergie (voir figure~\ref{fig:combine_power_generator}) pour inventer nos nouveaux tubes.

\begin{figure}[H]
    \centering
    \includegraphics[height=7cm]{combine_power_generator.jpg}
    \caption{Les générateurs d'énergie des Combine dans Half Life 2}
    \label{fig:combine_power_generator}
\end{figure}

\begin{wrapfigure}{R}{0.4\textwidth}
    \centering
    \includegraphics[width=0.38\textwidth]{newpipe_scheme.jpg}
    \caption{Schéma du nouveau tube}
    \label{fig:newpipe_scheme}
\end{wrapfigure}
Dans \emph{Half Life 2}, ces générateurs sont des sortes de tubes "électromagnétiques" contenant une ou des sphères d'énergie. Ces générateurs sont parfois protégés par un 
bouclier. Le joueur interagit avec les générateurs pour activer ou désactiver des objets. De plus, les sphères d'énergie peuvent également faire office de projectiles pour
abattre les ennemis. Sans aller jusqu'à ce dernier point, nous garderons l'aspect \emph{tube électromagnétique protégé par un bouclier et contenant des sphères d'énergie}.
Vous pouvez voir dans la figure~\ref{fig:newpipe_scheme} le schéma du nouveau tube. Nous mettons en place des boucliers (A et B sur la figure) qui s'ouvriront et se 
fermeront afin de jouer sur le temps d'apparition des sphères d'énergie.
\newpage

\begin{wrapfigure}[13]{L}{0.2\textwidth}
    \centering
    \includegraphics[width=0.18\textwidth]{newpipe.png}
    \caption{Visuel du nouveau tube, Blender et Unity}
    \label{fig:newpipe}
\end{wrapfigure}
J'ai réalisé et animé le tube sur \emph{Blender}, un logiciel de modélisation 3D.

\paragraph{}Une fois le tube créé, nous avons imaginé un système de barillet sur lequel reposent les tubes. Nous pourrons placer huit tubes sur ce barillet.
De même que le tube, le barillet fut modélisé sur \emph{Blender}.\\
Nous avons ensuite assemblé et texturé ces modèles sous \emph{Unity}. Nous avons procédé ainsi pour des raisons de simplicité: cette méthode nous a permis de garder
un plus grand contrôle sur les animations et la gestion individuelles des tubes.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{new_scene.png}
    \caption{Nouvelle scène}
    \label{fig:new_scene}
\end{figure}

\paragraph{Gameplay} Les mécaniques de jeu ont également évolué pour coller à cette nouvelle mise en scène. Le joueur ne peut plus interagir qu'avec le tube central.
Il doit ainsi faire <<tourner>> le barillet avec les touches <<gauche>> et <<droite>> afin de mettre le tube contenant la \emph{cible} au centre. Il utilisera alors la 
touche espace afin de retirer la \emph{cible}.Dance cette nouvelle version, les \emph{cibles} et les \emph{distracteurs} sont des sphères d'énergie. Les \emph{cibles} 
sont des sphères instables pouvant amener à la surcharge du tube (d'où la nécessité de les retirer du tube). Nous avons deux modes, un premier ou le niveau de stabilité 
est commun à tous les tubes, et un autre où il est calculé par tube. Si le niveau de stabilité est à zéro, le ou les tubes se ferment. Si tous les tubes sont fermés, le 
joueur perd. 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{PDC-barillier-evolution.jpg}
    \caption{Nouveau Gameplay}
    \label{fig:new_gameplay}
\end{figure}

Les tubes pouvant s'ouvrir sont situés à proximité de ceux déjà ouverts, si un seul tube est déjà ouvert, le nouveau s'ouvrira à une distance de 2 tubes maximum, si deux
déjà ouvert, le nouveau s'ouvrira directement adjacent aux autres tubes: soit au milieu, soit à gauche soit à droite. Ces possibilités sont décrites dans la 
figure~\ref{fig:opening_poss}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{PDC-open-tube.jpg}
    \caption{Possibilité d'ouverture de tube}
    \label{fig:opening_poss}
\end{figure}

Nous jouons également sur le contraste des sphères d'énergie afin de les rendre de plus en plus difficiles à différencier. La figure~\ref{fig:target_distractor_contrast}
l'évolution de ce contraste entre les deux extrêmes. À noter que la couleur des sphères étant générée par un effet graphique, l'image donne plus une idée générale qu'une
représentation fidèle.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{target_distractor_contrast.png}
    \caption{Contraste entre la \emph{cible} (gauche) et les \emph{distracteurs} (droite)}
    \label{fig:target_distractor_contrast}
\end{figure}

Afin de captiver le joueur, nous utilisons un système de <<scoring>> inspiré du jeu \emph{Osu}\cite{OSU}. Dans la figure~\ref{fig:scoring_osu}, on peut voir 4
éléments principaux de scoring. En haut à gauche se trouve une jauge de vie qui baisse à chaque erreur et remonte à chaque rythme réussi. En bas à gauche se 
trouve le combo de rythmes réussis à la suite. Celui-ci est un multiplicateur des points gagnés par rythme réussi. La somme totale des points est affichée en haut 
à droite, au-dessus du pourcentage de réussite sur la partie. Le scoring sur \emph{Osu} est détaillé sur leur site internet\cite{OSU}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{osu-scoring-inspiration-description.png}
    \caption{Le scoring dans \emph{Osu}}
    \label{fig:scoring_osu}
\end{figure}

Nous avons donc repris ces 4 éléments (voir figure~\ref{fig:scoring_pdc}) : il y a soit une jauge de vie qui est commune à tous les tubes, soit une jauge de vie 
pour chaque tube dans le carré noir en bas à gauche de l'écran. Si des \emph{cibles} passent, la jauge de vie baisse de 15. Si celles-ci sont retirées, la jauge remonte 
de 5. À chaque \emph{distracteur} qui passe, la barre de vie monte également de 0.1. Cela permet de faire remonter tout doucement la jauge de vie dans le cas où peu de 
\emph{cibles} apparaitraient. Au-dessus de la jauge de vie se trouvent le combo de réussite et le score. En bas à droite, la cible est représentée au joueur. La précision 
de réussite est juste au-dessus de la cible. 

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{PDC-scoring.png}
    \caption{Le scoring dans notre jeu}
    \label{fig:scoring_pdc}
\end{figure}

\paragraph{Évolution future} Plusieurs évolutions de ce mini jeu peuvent être envisagées. D'abord, nous pourrions utiliser des <<miroirs>> afin d'afficher les tuyaux 
derrière le barillet et ainsi utiliser plus de tubes. Nous pourrions aussi "transférer" des sphères d'énergie instable vers des tubes en manque d'énergie.

En ce qui concerne le jeu complet, plusieurs évolutions sont prévues:
\begin{itemize}
    \item L'ajout du mode multijoueur.
    \item L'ajout du panneau de configuration pour le professeur.
    \item La sauvegarde du profil et de la progression du joueur.
\end{itemize}

\subsection{Envoi et récupération des données}

Afin de suivre la progression du joueur et de valider l'effet de l'entraînement, nous devons récupérer les données retraçant la progression du joueur. Dans le cadre de 
l'étude, nous devons récupérer:
\begin{itemize}
    \item La tranche d'âge de l'utilisateur.
    \item Son genre (mâle, femelle, autre).
    \item La date du début de sa session et celle de fin.
    \item La date du début de sa partie et celle de fin.
    \item La date et le temps de réaction pour chaque \emph{Hit}.
    \item La date de chaque \emph{Miss}.
    \item La date de chaque \emph{FA}.
    \item La date d'apparition de chaque \emph{distracteur}.
    \item Le \emph{dprime}.
\end{itemize}

Afin de faciliter la classification de ses données, nous introduisons des données complémentaires:
\begin{itemize}
    \item Un identifiant de session.
    \item Un identifiant de partie.
\end{itemize}

À cela s'ajoute que nous devons pouvoir raccorder les parties à un joueur que nous pourrons identifié en temps que tel, mais nous ne devons pas identifier l'individu derrière
l'utilisateur. C'est-à-dire que nous devons pouvoir dire <<l'utilisateur \#42>,> mais pas <<Jean Kevin habitant à Avignon>>. Cette contrainte nous imposé par plusieurs 
organismes, incluant la \gls{cnil}.
Ces données doivent être récupérées par les chercheurs afin d'être analysées. Or, le jeu va être distribué hors de l'enceinte de l'\gls{ISC}. Il nous a semblé naturel, de 
par notre expérience avec des technologies web, de regarder la possibilité d'envoyé des requêtes \gls{http} à un serveur de l'\gls{ISC} afin de transmettre les données.
Nous sommes donc allés voir \bsc{Sylvain Marin} afin d'étudier la faisabilité de la chose et les contraintes qui y sont liées.

\paragraph{Contraintes} L'\gls{ISC} utilise des normes de sécurités qui empêche une libre circulation de la donnée entre son réseau interne et le réseau externe. Cette
contrainte nous demande donc de mettre en place une <<passerelle>> de communication entre le monde extérieur et le réseau interne de l'\gls{ISC}. \bsc{Sylvain} nous a 
aiguillés vers une solution. Il s'agirait de mettre en place un serveur \gls{http} tel que \emph{Apache} qui réceptionnerait des requêtes \gls{http_get}. Ces requêtes
seront alors enregistrées dans un journal du serveur \gls{http}. Il suffira alors de récupérer ce fichier et d'en extraire les données (voir 
figure~\ref{fig:diagram_data_sending}).

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{diagram_data_sending.png}
    \caption{Envoi et extraction des données}
    \label{fig:diagram_data_sending}
\end{figure}

Une autre contrainte à considérer est l'utilisation de ces données. Dans notre cas, les chercheurs souhaitent traiter toutes les données joueur par joueur.
Pour chaque joueur, il faudra pour pouvoir itérer sur chaque session et sur chaque partie auxquelles il aura joué.

\paragraph{Protocole d'envoi} Afin de pouvoir envoyer efficacement la donnée, nous avons dû créer un protocole d'envois. De par les limitations du \gls{http_get}, il
nous fallait une méthode simple pour envoyer une donnée précise.
C'est ainsi que nous avons mis en place un ensemble de \emph{verbes} qui permettront de définir quelle donnée est envoyée, et ainsi nous pourrons définir comment la lire.
Chaque requête est préfixée par <<ecas>> afin de pouvoir facilement la retrouver.
Voici la liste des verbes:
\begin{description}
    \item[RegisterPlayer] Au travers de ce verbe, nous indiquons l'apparition d'un nouveau joueur. Nous indiquerons son année de naissance, son genre ainsi que la date 
    d'enregistrement.\\
    Ex: RegisterPlayer/<HashPlayer>/<Year>/<Sexe>
    \item[CreateSession] Nous souhaitons savoir quand le joueur se connecte au jeu et s'il est seul.\\
    Ex: CreateSession/<HashSession>/ <Timestamp>/<IsAlone>
    \item[CloseSession] Cela indique quand le joueur quitte le jeu. Nous pouvons ainsi savoir combien de temps le joueur.\\
    Ex: CloseSession/<HashPlayer>/<HashSession>/ <Timestamp>
    \item[CreateParty] Nous souhaitons ici savoir quand le joueur comment un mini jeu et duquel il s'agit.\\
    Ex: CreateParty/<HashPlayer>/<HashSession>/<HashParty>/ <Timestamp>/<Game>
    \item[CloseParty] Similaire à <<CloseSession>>.\\
    Ex: CloseParty/<HashPlayer>/<HashSession>/<HashParty>/ <Timestamp>
    \item[RegisterHit] Le joueur à réussi à avoir une \emph{cible}, nous voulons savoir à quel moment, dans quel tuyau et combien de temps après son apparition.\\
    Ex: RegisterHit/<HashPlayer>/<HashSession>/<HashParty>/ <Timestamp>/<ReactionTime>/<Tube>
    \item[RegisterMiss] Si le joueur ne touche pas la \emph{cible}, nous indiquons à quel moment la cible disparait.\\
    Ex: RegisterMiss/<HashPlayer>/<HashSession>/<HashParty>/ <Timestamp>/<Tube>
    \item[RegisterDistractor] On enregistre toutes les créations de distracteurs avec le moment de création.\\
    Ex: RegisterDistractor/<HashPlayer>/<HashSession>/<HashParty>/ <Timestamp>/<Tube>
    \item[RegisterFA] Si le joueur appuie sur <<espace>> alors qu'il s'agit d'un distracteur. Encore une fois, nous voulons savoir quand.\\
    Ex: RegisterFA/<HashPlayer>/<HashSession>/<HashParty>/ <Timestamp>/<Tube>
    \item[RegisterDPrime] À intervalle régulier, nous envoyons le \emph{dprime} calculé. Nous souhaitons aussi savoir quand.\\
    Ex: RegisterDPrime/<HashPlayer>/<HashSession>/<HashParty>/ <Timestamp>/<Value>
\end{description}

Chaque action est porteuse de la date, nous permettant ainsi de reconstituer l'ordre chronologique de celles-ci.

\paragraph{Dans le jeu} Au sein du jeu, certains mécanismes ont dû être créés. D'abord, nous avons mis en place un page de connexion (voir figure~\ref{fig:login}). Elle
nous permet de récupérer plusieurs informations:
\begin{itemize}
    \item Le sexe du joueur.
    \item Son année de naissance.
    \item S'il joue seul.
    \i,tem Mais surtout son \emph{hash}.
\end{itemize}

Revenons un instant sur le \emph{hash}. Nous l'avions déjà évoqué précédemment. Un \emph{hash} est une valeur précise, souvent une chaîne de caractères, calculé à partir
d'autres valeurs. Un hash possède une propriété intéressante par cela qu'il permet de s'assurer de l'égalité de plusieurs objets sans avoir à les comparer entre eux, mais
en comparant leur \emph{hash}. Ajoutons à cela que la fonction de \emph{hachage} peut être faite de telle manière que la donnée à l'origine du \emph{hash} ne puisse être 
calculée à partir de celui-ci.
Nous utilisons plusieurs \emph{hash} dans notre jeu. La plupart sont uniquement utilisés pour identifier une entité (partie, session). Mais dans le cas du joueur, son
\emph{hash} est généré à partir de son nom d'utilisateur et de son mot de passe. Ainsi, nous ne pouvons l'identifier formellement, mais son identifiant permet quand même
de suivre son évolution.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{login-screen.png}
    \caption{Page de connexion utilisateur}
    \label{fig:login}
\end{figure}

\paragraph{Outil d'extraction} L'outil d'extraction que nous avons réalisé se présente sous la forme d'une \gls{cli}. Il prend en paramètre le fichier duquel nous
extrayons la donnée et en sortie le dossier ou nous sortirons un fichier \gls{json} pour chaque utilisateur. Nous avons utilisé le langage \emph{Rust}
\footnote{\url{https://www.rust-lang.org/fr-FR/}} et ce pour plusieurs raisons:
\begin{itemize}
    \item C'est un langage qui interdit au travers de son compilateur beaucoup d'erreurs. Cette particularité nous permet d'assurer que les chercheurs ou futurs 
    développeurs travaillant sur ce projet ne pourront pas écrire un code incorrect facilement. En contrepartie, l'évolution de cet extracteur sera plus longue.
    \item C'est un langage naturellement multiplateforme.
    \item C'est un langage compilé qui fournit un exécutable après construction. Il n'y a donc nul besoin d'une <<machine virtuelle>> tel que la \emph{JVM} pour 
    Java ou l'\emph{interpréteur de Python} pour Python.   
\end{itemize}

\paragraph{} Cet outil fonctionne en deux temps et s'appuie sur une structure de données intermédiaire. Nous extrayons d'abord la donnée vers cette structure interne. 
De cette manière, l'extraction se fait de manière indépendante au format de sortie.
Cette première étape est elle même en deux parties. On récupère d'abord les \emph{verbes} définis précédemment en utilisant des expressions régulières pour les mettre 
dans une liste. On vient ensuite construire de manière incrémentale notre structure interne à partir des \emph{verbes}.
Dans un deuxième temps, nous exportons cette structure interne vers des \gls{json}. Pour cette partie, nous avons joué la carte de la simplicité et utilisé la 
bibliothèque de sérialisation \emph{Serde}\footnote{\url{https://serde.rs/}} qui prend lui-même en charge l'export. Nous exportons un fichier par joueur, chaque 
fichier contient l'âge, le sexe et le \emph{hash} du joueur. Il contient également la liste de chaque session du joueur et pour chacune d'entre elles, les parties 
effectuées par celui-ci.
La figure~\ref{fig:parser_internal} résume ce que nous venons de décrire.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{parser_internal.png}
    \caption{Fonctionnement interne de l'extracteur}
    \label{fig:parser_internal}
\end{figure}

\paragraph{Évolution future} Cet outil est amené à évoluer. Voici quelque piste:
\begin{description}
    \item[Vers un outil serveur] On peut imaginer que cet outil pourrait devenir un serveur \emph{Standalone}, c'est-à-dire qu'il pourrait fonctionner seul, sans le
    concours d'une autre application, et recevoir les \emph{verbes} directement.
    \item[Intégration d'une base de données] La structure de données interne utilisée par l'extracteur pourrait être déportée vers une base de données. Cela faciliterait
    le suivie de la donnée.
    \item[Supporter plus de formats] Dans le futur,  application pourrait supporter plus de formats en entré comme en sortie. Nous pourrions lire des journaux d'autre
    serveur \gls{http} comme \emph{Nginx} et exporter vers du \gls{xml} par exemple.
\end{description}

\subsection{Résultats}

Nous avons vu dans la section~\ref{sssec:cpt} que la tache de \emph{CPT} permettait d'entraîner l'attention d'un sujet. Cet entraînement est mesurable au travers des
\emph{Hits}, \emph{Miss}, \emph{FAs} et du \emph{DPrime}. Notre mini jeu (voir section~\ref{ssec:dev_game}) reprend l'expérience dans son protocole et
envoi les mêmes données. De plus, bien que la tache de \emph{CPT} soit orientée sur l'entraînement de l'attention continue et de l'attention sélective, notre jeu peut 
également se diversifier vers l'attention divisive et l'attention spatiale (voir section~\ref{sssec:def}).
\newpage